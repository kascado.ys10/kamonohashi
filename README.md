# kamonohashi

## コンテナビルド

```$ docker build -t yuta4j1/webui-node:1.0 .```

## コンテナ実行

```$ docker run -d -p 1234:1234 --name webui-node yuta4j1/webui-node:1.0```
