import Fastify from 'fastify'
import cors from 'fastify-cors'
import fastifyStatic from 'fastify-static'
import path from 'path'

const fastify = Fastify({
  logger: true
})

fastify.register(cors)
fastify.register(require('fastify-static'), {
  root: __dirname,
  prefix: '/' // optional: default '/'
})

// チャンネル所属のメンバー取得
fastify.get('*', async (request, reply) => {
  const chMembers = []
  return reply.sendFile('index.html')
})

fastify.listen(1234, '0.0.0.0', function(err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})
